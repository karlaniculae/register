package exercitiul1;

import java.util.ArrayList;
import java.util.Scanner;

public class Registration {

	public static void main(String[] args) {

		Scanner sc;
		String name = "Start";
		int age = 0, count = 0, ok = 0;
		ArrayList<String> Name = new ArrayList<String>();
		ArrayList<Integer> Age = new ArrayList<Integer>();

		while (ok == 0) {
			sc = new Scanner(System.in);
			System.out.println("Please tell me your name:");
			name = sc.nextLine();
			if (name.equals("EXIT")) {
				ok = 1;
				break;
			} else {
				sc = new Scanner(System.in);
				System.out.println("Please tell me your age:");
				age = sc.nextInt();
				System.out.println("Thank you " + name + " for your registration!");
			}

			Name.add(name);
			Age.add(age);
			count += 1;

		}
		System.out.println("Below you can find a list with " + count + "registered persons:");
		System.out.println("Name   Age");
		for (int i = 0; i < Name.size(); i++) {
			System.out.println(Name.get(i) + "   " + Age.get(i));
		}

	}
}
